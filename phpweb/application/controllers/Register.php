<?php


class Register extends CI_Controller
{
    public function reguser(){
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Lastname Name', 'required');
        $this->form_validation->set_rules('dob', 'Date of Birth', 'required');
        $this->form_validation->set_rules('username', 'User Name', 'required|is_unique[users.username]|min_length[5]|max_length[12]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Register');
        }
        else
        {
            $this->load->model('user');
            $response = $this->user->instertuserdata();

            if($response){

                $this->session->set_flashdata('msg','Successfully registered..');
                redirect('Welcome/viewlogin');

            }else{
                $this->session->set_flashdata('msg','Something Went wrong..');
                redirect('Welcome/viewregister');
            }
        }

    }
}