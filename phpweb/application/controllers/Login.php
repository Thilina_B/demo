<?php


class Login extends CI_Controller
{
    function loginuser(){

        $this->form_validation->set_rules('username', 'User Name', 'required|min_length[5]|max_length[12]');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Login');
        }else{

            $this->load->model('user');
            $result = $this->user->loginuser();
              if($result != false){

                  $userdata = array(
                      'uid'=>$result->id,
                      'firstname'=>$result->fname,
                      'lastname'=>$result->lname,
                      'dob'=>$result->dob,
                      'gender'=>$result->gender,
                      'loggedin'=>TRUE
                  );


                  //$this->session->set_userdata($userdata);

                  $this->session->set_tempdata($userdata,5);
                  $this->session->set_flashdata('welcome','Welcome');
                  redirect('Home/index');



              }else{
                  $this->session->set_flashdata('error','Invalid credentials');
                  redirect('Welcome/viewlogin');
              }

        }

    }
}